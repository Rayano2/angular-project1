import { Component, OnInit } from '@angular/core';
import {Recipe} from '../recipe.moel';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
    recipe: Recipe[] =  [new Recipe('A Test Recipe', 'This is a simply Test',
      'https://pinchofyum.com/wp-content/uploads/Buffalo-Cauliflower-Tacos-with-Avocado-Crema-Recipe.jpg'),
      new Recipe('A Test Recipe 2', 'Fotshini' , 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/The_Only_Original_Alfredo_Sauce_with_Butter_and_Parmesano-Reggiano_Cheese.png/220px-The_Only_Original_Alfredo_Sauce_with_Butter_and_Parmesano-Reggiano_Cheese.png')];

  constructor() { }

  ngOnInit() {
  }

}
