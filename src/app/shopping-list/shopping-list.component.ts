import { Component, OnInit } from '@angular/core';
import {IntgredtedModel} from '../Shared/Intgredted.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ShoopingList: IntgredtedModel [] = [new IntgredtedModel('Apple', 5),
                                       new IntgredtedModel('Orange', 2)];
  constructor() { }

  ngOnInit() {
  }

}
